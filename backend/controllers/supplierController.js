const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

var { Supplier } = require('../models/supplier');

// => localhost:4444/suppliers/
router.get('/', (req, res) => {
    Supplier.find((err, docs) => {
        if (!err) { res.send(docs); }
        else { console.log('Error in Retriving Supplier :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Supplier.findById(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Retriving Supplier :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.post('/', (req, res) => {
    var supplier = new Supplier({
        kodeSupplier: req.body.kodeSupplier,
        namaSupplier: req.body.namaSupplier,
        alamat: req.body.alamat,
        tanggalLahir: req.body.tanggalLahir,
        telepon: req.body.telepon,
   
    });
    supplier.save((err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Supplier Save :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.put('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

        var supplier = {
            kodeSupplier: req.body.kodeSupplier,
            namaSupplier: req.body.namaSupplier,
            alamat: req.body.alamat,
            tanggalLahir: req.body.tanggalLahir,
            telepon: req.body.telepon,
        };
    Supplier.findByIdAndUpdate(req.params.id, { $set: supplier }, { new: true }, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Supplier Update :' + JSON.stringify(err, undefined, 2)); }
    });
});

router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No record with given id : ${req.params.id}`);

    Supplier.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) { res.send(doc); }
        else { console.log('Error in Supplier Delete :' + JSON.stringify(err, undefined, 2)); }
    });
});

module.exports = router;