const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { mongoose } = require('./db_connect.js');
var productController = require('./controllers/productController.js');
var supplierController = require('./controllers/supplierController.js');

var app = express();
app.use(bodyParser.json());
app.use(cors({ origin : 'http://localhost:4200'}));

app.listen(4444, () => console.log('Server started at port : 4444'));

app.use('/products', productController);
app.use('/suppliers', supplierController);