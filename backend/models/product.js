const mongoose = require('mongoose');

var Product = mongoose.model('Product', {
    kodeBarang: { type: String },
    namaBarang: { type: String },
    stok: { type: Number },
    tanggalMasuk: {type: Date},
    harga: {type: Number}
});
module.exports = {Product};