const mongoose = require('mongoose');

var Supplier = mongoose.model('Supplier', {
    kodeSupplier: { type: String },
    namaSupplier: { type: String },
    alamat: { type: String },
    tanggalLahir: {type: Date},
    telepon: {type: String}
});
module.exports = {Supplier};