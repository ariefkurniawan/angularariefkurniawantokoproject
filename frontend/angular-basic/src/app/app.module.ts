import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule} from '@angular/http';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { ProductService } from './service/product.service';
import { FormGroup, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ControlSidebarComponent } from './components/control-sidebar/control-sidebar.component';
import { ProductComponent } from './toko/product/product.component';
import { DashboardComponent } from './toko/dashboard/dashboard.component';
import { EditProductComponent } from './toko/product/edit-product/edit-product.component';
import { AddProductComponent } from './toko/product/add-product/add-product.component';
import { SupplierComponent } from './toko/supplier/supplier.component';
import { AddSupplierComponent } from './toko/supplier/add-supplier/add-supplier.component';
import { EditSupplierComponent } from './toko/supplier/edit-supplier/edit-supplier.component';
import { SupplierService } from './service/supplier.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'product',
    component: ProductComponent
  },
  {
    path: 'product/add-product',
    component: AddProductComponent
  },
  {
    path: 'product/edit-product',
    component: EditProductComponent
  },
  {
    path: 'supplier',
    component: SupplierComponent
  },
  {
    path: 'supplier/add-supplier',
    component: AddSupplierComponent
  },
  {
    path: 'supplier/edit-supplier',
    component: EditSupplierComponent
  }
  ];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    ControlSidebarComponent,
    ProductComponent,
    DashboardComponent,
    EditProductComponent,
    AddProductComponent,
    SupplierComponent,
    AddSupplierComponent,
    EditSupplierComponent
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)

  ],
  providers: [ProductService, SupplierService],
  bootstrap: [AppComponent]
})
export class AppModule { }
