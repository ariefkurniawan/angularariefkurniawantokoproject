import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../../service/product.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  simpanDataProduct: any;
  constructor(private router:Router, private service:ProductService) { }

  ngOnInit() {
    
  }

  onSubmit()
  {
    this.simpanDataProduct =
    {
      "kodeBarang": (document.getElementById("kodeBarang") as HTMLInputElement).value,
      "namaBarang": (document.getElementById("namaBarang") as HTMLInputElement).value,
      "stok": (document.getElementById("stok") as HTMLInputElement).value,
      "tanggalMasuk": (document.getElementById("tanggalMasuk") as HTMLInputElement).value,
      "harga": (document.getElementById("harga") as HTMLInputElement).value
      
    }
    this.service.addProduct(this.simpanDataProduct).subscribe((res) => {
      console.log(res);
      // alert("Data Sukses Disimpan");
      this.router.navigate(['product']);
    });
  }

  backPage()
  {
    this.router.navigate(['product']);
  }
}
