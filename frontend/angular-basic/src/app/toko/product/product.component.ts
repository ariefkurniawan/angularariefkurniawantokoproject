import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../service/product.service';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  
  dataProduct : any;
  //simpanDataProduct:any;

  constructor(private router:Router, private service:ProductService) { }

  ngOnInit() {
    this.service.getProduct().subscribe(data => {
      return this.dataProduct = (data);
    });
    
  }

  // onSubmit()
  // {
  //   this.simpanDataProduct =
  //   {
  //     "kodeBarang": (document.getElementById("kodeBarang") as HTMLInputElement).value,
  //     "namaBarang": (document.getElementById("namaBarang") as HTMLInputElement).value,
  //     "stok": (document.getElementById("stok") as HTMLInputElement).value,
  //     "tanggalMasuk": (document.getElementById("tanggalMasuk") as HTMLInputElement).value,
  //     "harga": (document.getElementById("harga") as HTMLInputElement).value
      
  //   }
  //   this.service.addProduct(this.simpanDataProduct).subscribe((res) => {
  //     console.log(res);
  //     this.router.navigate(['product']);
  //   });
  // }

  editProduct(product):void
  {
    console.log(product);
    window.localStorage.removeItem("editProductId");
    window.localStorage.setItem("editProductId", product._id + "");
    this.router.navigate(['product/edit-product']);
  }

  deleteProduct(product):void
  {
    console.log(product);
    this.service.deleteProduct(product).subscribe((res) => {
      this.service.getProduct().subscribe(data => 
        {
          return this.dataProduct = data;
        });
    });
  }

  //button
  tambah()
  {
    this.router.navigate(['product/add-product']);
  }

}
