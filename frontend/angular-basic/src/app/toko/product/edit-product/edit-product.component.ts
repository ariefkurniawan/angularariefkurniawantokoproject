import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../../../service/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  editForm: FormGroup;
  constructor(private router:Router, private service:ProductService, private formBuilder:FormBuilder) { }

  ngOnInit() {
    let productId = window.localStorage.getItem("editProductId");
    if(!productId)
    {
      this.router.navigate(['product']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id:[],
      kodeBarang:[],
      namaBarang:[],
      stok:[],
      tanggalMasuk:[],
      harga:[],
      __v:[]
    });
    this.service.getProductById(productId).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit()
  {
    this.service.editProduct(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Success");
        this.router.navigate(['product']);
      } catch (err) {
        console.log(err.message);
      }
    });
    
  }

  //button
  back()
  {
    this.router.navigate(['product']);
  }

}
