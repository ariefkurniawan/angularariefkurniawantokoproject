import { Component, OnInit } from '@angular/core';
import { SupplierService } from '../../../service/supplier.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.css']
})
export class AddSupplierComponent implements OnInit {

  simpanDataSupplier:any;
  constructor(private serviceSupplier: SupplierService, private router: Router) { }

  ngOnInit() {
  }

  addSuppliers()
  {
    this.simpanDataSupplier =
    {
      "kodeSupplier": (document.getElementById("kodeSupplier") as HTMLInputElement).value,
      "namaSupplier": (document.getElementById("namaSupplier") as HTMLInputElement).value,
      "alamat": (document.getElementById("alamat") as HTMLInputElement).value,
      "tanggalLahir": (document.getElementById("tanggalLahir") as HTMLInputElement).value,
      "telepon": (document.getElementById("telepon") as HTMLInputElement).value
      
    }
    this.serviceSupplier.addSupplier(this.simpanDataSupplier).subscribe((res) => {
      console.log(res);
      //alert("Data Sukses Disimpan");
      this.router.navigate(['supplier']);
    });
  }

  backPage()
  {
    this.router.navigate(['supplier']);
  }
}
