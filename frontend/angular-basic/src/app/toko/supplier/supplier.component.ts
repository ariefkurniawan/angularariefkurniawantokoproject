import { Component, OnInit } from '@angular/core';
import { SupplierService } from '../../service/supplier.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {

  dataSupplier : any;

  constructor(private serviceSupplier: SupplierService, private router: Router ) { }

  ngOnInit() {
    this.serviceSupplier.getSupplier().subscribe(data => {
      return this.dataSupplier = (data);
    });
  }

  editSupplier(supplier):void
  {
    console.log(supplier);
    window.localStorage.removeItem("editSupplierId");
    window.localStorage.setItem("editSupplierId", supplier._id + "");
    this.router.navigate(['supplier/edit-supplier']);
  }

  deleteSupplier(supplier):void
  {
    console.log(supplier);
    this.serviceSupplier.deleteSupplier(supplier).subscribe((res) => {
      this.serviceSupplier.getSupplier().subscribe(data => 
        {
          return this.dataSupplier = data;
        });
    });
  }

  //button
  tambahSupplier():void
  {
    this.router.navigate(['supplier/add-supplier']);
  }

}
