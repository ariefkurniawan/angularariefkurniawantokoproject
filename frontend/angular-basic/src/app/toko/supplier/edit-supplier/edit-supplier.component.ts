import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { SupplierService } from '../../../service/supplier.service';

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.css']
})
export class EditSupplierComponent implements OnInit {

  editForm: FormGroup;
  constructor(private router:Router, private serviceSupplier:SupplierService, private formBuilder:FormBuilder) { }

  ngOnInit() {
    let supplierId = window.localStorage.getItem("editSupplierId");
    if(!supplierId)
    {
      this.router.navigate(['supplier']);
      return;
    }

    this.editForm = this.formBuilder.group({
      _id:[],
      kodeSupplier:[],
      namaSupplier:[],
      alamat:[],
      tanggalLahir:[],
      telepon:[],
      __v:[]
    });
    this.serviceSupplier.getSupplierById(supplierId).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  onSubmit()
  {
    this.serviceSupplier.editSupplier(this.editForm.value).subscribe((data) => {
      try {
        alert("Update Success");
        this.router.navigate(['supplier']);
      } catch (err) {
        console.log(err.message);
      }
    });
    
  }

  //button
  back()
  {
    this.router.navigate(['supplier']);
  }

}
