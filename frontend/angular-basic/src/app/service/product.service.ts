import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; //tambahan
import { Observable } from 'rxjs';
import { Product } from '../model/product.model';

@Injectable()
export class ProductService {

  BASE_URL:string='http://localhost:4444/products/';
  constructor(private http: HttpClient) { }

  getProduct(): Observable<any>
  {
    return this.http.get<any>(this.BASE_URL);
  }

  getProductById(productId : string): Observable<any>
  {
    return this.http.get<any>(this.BASE_URL+productId);
  }

  addProduct(product: Product)
  {
    return this.http.post(this.BASE_URL, product);
  }

  editProduct(product: Product)
  {
    return this.http.put(this.BASE_URL+product._id, product);
  }

  deleteProduct(product)
  {
    return this.http.delete(this.BASE_URL+product._id);
  }

}
