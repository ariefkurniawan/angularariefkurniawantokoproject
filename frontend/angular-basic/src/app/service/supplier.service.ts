import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; //tambahan
import { Observable } from 'rxjs';
import { Supplier } from '../model/supplier.model';

@Injectable()
export class SupplierService {

  BASE_URL:string='http://localhost:4444/suppliers/';
  constructor(private http: HttpClient) { }

  getSupplier(): Observable<any>
  {
    return this.http.get<any>(this.BASE_URL);
  }

  getSupplierById(supplierId : string): Observable<any>
  {
    return this.http.get<any>(this.BASE_URL+supplierId);
  }

  addSupplier(supplier: Supplier)
  {
    return this.http.post(this.BASE_URL, supplier);
  }

  editSupplier(supplier: Supplier)
  {
    return this.http.put(this.BASE_URL+supplier._id, supplier);
  }

  deleteSupplier(supplier)
  {
    return this.http.delete(this.BASE_URL+supplier._id);
  }

}
