export class Product
{
    _id: String;
    kodeBarang: String;
    namaBarang: String;
    stok: Number;
    tanggalMasuk: Date;
    harga: Number;
}