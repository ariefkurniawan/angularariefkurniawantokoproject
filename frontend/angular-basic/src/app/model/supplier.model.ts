export class Supplier
{
    _id: String;
    kodeSupplier: String;
    namaSupplier: String;
    alamat: String;
    tanggalLahir: Date;
    telepon: String;
}